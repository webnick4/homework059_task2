import React, { PureComponent } from 'react';
import './Post.css';

class Post extends PureComponent {

  render() {
    return (
      <article className="Post">
        <p>{this.props.posts}</p>
      </article>
    );
  }
}
export default Post;