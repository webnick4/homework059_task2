import React, { Component, Fragment } from 'react';
import './JokesGenerator.css';
import Post from "../../components/Post/Post";

class JokesGenerator extends Component {
  state = {
    posts: []
  };

  componentDidMount() {

    const POSTS_URL = 'https://api.chucknorris.io/jokes/random';

    fetch(POSTS_URL).then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Something went wrong with network request');
    }).then(posts => {
      const postsArray = [];
      const createObjPost = {id: posts.id, value: posts.value};

      postsArray.push(createObjPost);

      const updatedPosts = postsArray.map(post => {
        return post.value;
      });

      this.setState({posts: updatedPosts});
    }).catch(error => {
      console.log(error);
    })
  }

  render() {
    return (
      <Fragment>
        <section className="Posts">
          <Post className="Post" posts={this.state.posts} />
        </section>
      </Fragment>
    )
  }
}

export default JokesGenerator;